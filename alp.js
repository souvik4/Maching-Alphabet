/*let arr = "one, two, three";
let len = arr.length;
let newArr = arr.split(" ");
console.log(newArr);
console.log(len);
let word = arr.search("h");
console.log(word);*/

const str = "oneseveneighttwotenthreefouronefivestwoixthreesevennineeightninrten";

let shuffle = (str) =>
    [...str].reduceRight( // executes the callback function once for each element
            (res,_, __, arr) => 
                (res.push(...arr.splice(0 | (Math.random() * arr.length) , 1)), res),[]
        ).join("");

const shuffStr = shuffle(str);
console.log(shuffStr);

let num = [0];
let result = [];

let counter = (c) => {
    return (shuffStr.match(/${c}/gi) || []).length; //The gi modifier is used to do a case insensitive search of all occurrences of a regular expression in a string
}


num[1] = counter["w"];//2
num[3] = counter["u"];//4
num[5] = counter["x"];//6
num[7] = counter["g"];//8
num[9] = counter["n"];//10
num[0] = counter["o"] - num[1] - num[3];//1
num[2] = counter["h"] - num[7];//3
num[4] = counter["f"] - num[3];//5
num[6] = counter["s"] - num[5] - num[9];//7
num[8] = counter["i"] - num[4] - num[5] - num[7];//9

for(let i=0; i<num.length; i++){
    result.push(shuffStr[i] * num[i]);
}

/*for(let i=0; i<str.length; i++){
    result.push(shuffStr[i] );
}*/

/*for(let i=0; i<num.length; i++){
    result.push(num[i] );
}*/

console.log(result);

let one=0, two=0, three=0, four=0, five=0, six=0, seven=0, eight=0, nine=0, ten=0;

if (shuffStr.includes("w",0)){
    two = (shuffStr.match(/w/gi) || []).length;
}

if (shuffStr.includes("u",0)){
    four = (shuffStr.match(/u/gi) || []).length;
}

if (shuffStr.includes("x",0)){
    six = (shuffStr.match(/x/gi) || []).length;
}

if (shuffStr.includes("g",0)){
    eight = (shuffStr.match(/g/gi) || []).length;
}

if (shuffStr.includes("n",0)){
    ten = (shuffStr.match(/n/gi) || []).length;
}

if (shuffStr.includes("o",0)){
    one = (shuffStr.match(/o/gi) || []).length;
}

if (shuffStr.includes("h",0)){
    three = (shuffStr.match(/h/gi) || []).length;
}

if (shuffStr.includes("f",0)){
    five = (shuffStr.match(/f/gi) || []).length;
}

if (shuffStr.includes("s",0)){
    seven = (shuffStr.match(/s/gi) || []).length;
}

if (shuffStr.includes("i",0)){
    nine = (shuffStr.match(/i/gi) || []).length;
}